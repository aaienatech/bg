# API for Background Removal App
This is an api for background removal app.

# Steps involved
1. Go to 
    https://www.kaggle.com/oishee30/cocopersonsegmentation
    https://www.kaggle.com/furkankati/person-segmentation-dataset?select=Training
    https://www.kaggle.com/tapakah68/supervisely-filtered-segmentation-person-dataset?select=supervisely_person_clean_2667_img
    https://www.kaggle.com/oishee30/cocopersonsegmentation?select=train2017_ann

2. Go to 
    https://www.kaggle.com/docs/api
    https://www.kaggle.com/general/74235

3. Go to
    https://www.tensorflow.org/api_docs/python/tf/data/Dataset
    https://keras.io/examples/vision/oxford_pets_image_segmentation/

4. Combined Masking and Numpy manipulations.