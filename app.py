import os
from skimage import io
from werkzeug.exceptions import BadRequest
from flask import Flask, render_template, request, Response, send_file
from utils import save_img, load_img, bg_remove

upload_folder = "static"
app = Flask(__name__)


@app.route("/removebg", methods= ['POST', 'GET'])
def upload_file():
    if request.method == "POST":
        image_file = request.files["image"]
        if image_file:
            image_location = os.path.join(upload_folder, image_file.filename)
            image_file.save(image_location)

            new_image = load_img(image_location)
            new_image_name = 'new_'+image_file.filename+'.jpg'
            new_image_location = save_img(new_image, new_image_name, upload_folder)

            pred = bg_remove(imgpath=new_image_location, img=None)
            pred_image_name = 'pred_'+image_file.filename
            pred_location = save_img(pred, pred_image_name, upload_folder)            

            return send_file(pred_location, mimetype='image/gif')
        else:
            return "Image not found. Please upload the image."
    else:
        "Something bad happened. Please try again."

if __name__ == "__main__":
    app.run()